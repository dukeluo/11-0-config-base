package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class PropertyTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_env() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/env"))
                .andExpect(MockMvcResultMatchers.content().string("This is really great"));
    }
}
