package com.twuc.webApp.Controller;

import com.twuc.webApp.Properties.EnvProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

//    ### 1
//    @Value("${twuc.webapp.name}")
//    private String env;

//    ### 2
//    private Environment environment;
//
//    public Controller(Environment environment) {
//        this.environment = environment;
//    }
//

    private EnvProperty envProperty;

    public Controller(EnvProperty envProperty) {
        this.envProperty = envProperty;
    }

    @GetMapping(value = "/api/env")
    public String getEnv() {
//        return env;
//        return environment.getProperty("twuc.webapp.name");
        return envProperty.getName();
    }
}
